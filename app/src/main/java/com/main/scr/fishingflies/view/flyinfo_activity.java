package com.main.scr.fishingflies.view;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.main.scr.fishingflies.R;

import static com.main.scr.fishingflies.R.id.imageView_fly;
import static com.main.scr.fishingflies.R.id.textView_fly_properties;
import static com.main.scr.fishingflies.R.layout.content_flyinfo_activity;

public class flyinfo_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flyinfo_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**
         * Get info from SQLite database fly by fly
         * */
        TextView myTextview_frame = (TextView) findViewById(R.id.textView_frame_left);
        myTextview_frame.setBackgroundColor(Color.parseColor("#bfbfbf"));

        TextView myTextview_fly_properties = (TextView) findViewById(R.id.textView_fly_properties);
        myTextview_fly_properties.setMovementMethod(new ScrollingMovementMethod());
        myTextview_fly_properties.setBackgroundColor(Color.parseColor("#bfbfbf"));
        myTextview_fly_properties.setText(Html.fromHtml(
                "<br><b>Fångar:</b>"
                + "<br> | Havsöring</br>"
                + "<br> | Regnbåge</br>"

                + "<br><br><b>Vattentyp:</b></br>"
                + "<br> | Stilla</br>"
                + "<br> | Strömmande</br>"
                + "<br> | Kusten</br>"

                + "<br><br><b>Vattentemperatur:</b></br>"
                + "<br> | 15 - 20 C</br>"
                + "<br> | 10 - 15 C</br>"
                + "<br> | 5 - 10 C</br>"
                + "<br> | 0 - 5 &nbsp;&nbsp;C</br>"

                + "<br><br><b>Fiskeperiod:</b></br>"
                + "<br> | jan - jun </br>"
                + "<br> | sep - dec </br>"

                + "<br><br><b>Fisketid:</b></br>"
                + "<br> | Dag</br>"
                + "<br> | Morgon/Kväll</br>"
                + "<br> | Natt</br>"

                + "<br><br><b>Fiskedjup:</b></br>"
                + "<br> | Ytan</br>"
                + "<br> | Mellanskiktet</br>"
                + "<br> | Botten</br>"

                + "<br><br><b>Infiskning:</b></br>"
                + "<br> | Långsam</br>"
                + "<br> | Snabb</br>"
                + "<br> | Ryckvis</br><br>"

        ));

        TextView textView_fly_general = (TextView) findViewById(R.id.textView_fly_general_text);
        textView_fly_general.setMovementMethod(new ScrollingMovementMethod());
        textView_fly_general.setBackgroundColor(Color.parseColor("#ffffff"));
        textView_fly_general.setText(Html.fromHtml(
                "<b>Muddler</b>"
                 + "<br>Orginalet Muddler Minnow var tänkt som en simpaimitation, men idag "
                 + "har vi många fler varianter. Muddlers effektivitet på storöring är sedan "
                 + "länge väldokumenterad. Vi har valt Muddler som samlingnamn för alla dessa "
                 + "olika flugor med hjorthårshuvud.</br>"

                 + "<br><br><b>Fisketips:</b>"
                 + "<br>Beskriver hur flugan fiskas och om flugan</br>"
        ));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
