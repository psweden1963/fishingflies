package com.main.scr.fishingflies.database;

/**
 * Created by PETALB on 2017-09-27.
 *
 * STEG 1:
 * Fyll på databasen med information
 *
 * STEG 2:
 * Strängen som ska sättas samman ==> fly_properties
 * 1. Hämta data från dataabasen
 * 2. Spara den information som finns tillgänglig i en array av strängar.
 * 3. Sätt samman strängarna och visa informationen genom att returnera strängarna.
 *
 */

public class db_GetData {

    String headerGeneral, headerGeneralText;
    String headerFishInfo, hederFishInfoText;
    String cathed, waterType, waterDegree, fishPeriod, fishtime, fishdeep, fishing;
    String br ="<br>", brEnd="</br>", bold = "<b>", boldEnd = "</b>";
    String li = " | ", colon = ":";
    char citat = '"';


    private String formatedString(){



        return "";
    }

    String test = citat + br + bold + cathed + boldEnd + brEnd + citat ;

   String fly_properties =
           "<br><b>Fångar:</b>"
            + "<br> | Havsöring</br>"
            + "<br> | Regnbåge</br>"

            + "<br><br><b>Vattentyp:</b></br>"
            + "<br> | Stilla</br>"
            + "<br> | Strömmande</br>"
            + "<br> | Kusten</br>"

            + "<br><br><b>Vattentemperatur:</b></br>"
            + "<br> | 15 - 20 C</br>"
            + "<br> | 10 - 15 C</br>"
            + "<br> | 5 - 10 C</br>"
            + "<br> | 0 - 5 &nbsp;&nbsp;C</br>"

            + "<br><br><b>Fiskeperiod:</b></br>"
            + "<br> | jan - jun </br>"
            + "<br> | sep - dec </br>"

            + "<br><br><b>Fisketid:</b></br>"
            + "<br> | Dag</br>"
            + "<br> | Morgon/Kväll</br>"
            + "<br> | Natt</br>"

            + "<br><br><b>Fiskedjup:</b></br>"
            + "<br> | Ytan</br>"
            + "<br> | Mellanskiktet</br>"
            + "<br> | Botten</br>"

            + "<br><br><b>Infiskning:</b></br>"
            + "<br> | Långsam</br>"
            + "<br> | Snabb</br>"
            + "<br> | Ryckvis</br><br>";



    String fly_general_info =

            citat + bold + headerGeneral + colon + boldEnd + citat
            + citat + br + headerGeneralText + brEnd + citat
            + citat + br + br + bold + headerFishInfo + colon + boldEnd + citat
            + citat + br + hederFishInfoText + brEnd + citat;


    }
